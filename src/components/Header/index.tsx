import {
  Box,
  Container,
  Flex,
  Icon,
  IconButton,
  Input,
  InputGroup,
  InputRightElement,
  Select,
  Text,
} from "@chakra-ui/react";
import { FaSearch, FaSpaceShuttle, FaTimes } from "react-icons/fa";
import { HeaderProps } from "../../services/interfaces";

export const Header = ({
  inputValue,
  setInputValue,
  submit,
  handleSort,
}: HeaderProps) => {
  return (
    <Container
      maxW="1280px"
      borderBottom="1px solid"
      borderColor="secondary.normal"
    >
      <Flex justify="space-between" align="center">
        <Flex justify="center" align="center">
          <Box rotate="45deg">
            <Icon
              border="1px solid"
              borderRadius="100%"
              margin={5}
              w="60px"
              h="60px"
              padding="5px"
              as={FaSpaceShuttle}
              transform="rotate(-45deg)"
              color="primary.normal"
              borderColor="secondary.normal"
            />
          </Box>
          <Text fontWeight="bold" fontSize="2rem" color="primary.normal">
            Space Flight News
          </Text>
        </Flex>
        <Flex>
          <InputGroup
            as="form"
            onSubmit={submit}
            position="relative"
            marginRight="20px"
          >
            <Input
              placeholder="Pesquise aqui..."
              value={inputValue}
              paddingRight={20}
              variant="outline"
              borderColor="secondary.normal"
              _hover={{
                borderColor: "secondary.light",
              }}
              _focus={{
                boxShadow: "0 0 0 1px var(--chakra-colors-primary-normal)",
                borderColor: "primary.normal",
              }}
              onChange={(e) => setInputValue(e.target.value)}
            />
            <InputRightElement>
              <IconButton
                aria-label="Filter results"
                icon={<FaSearch />}
                type="submit"
                position="absolute"
                right={0}
              />
              {!!inputValue && (
                <IconButton
                  aria-label="Clear filter"
                  variant="ghost"
                  icon={<FaTimes />}
                  color="gray.200"
                  bgColor="unset"
                  position="absolute"
                  _hover={{ bgColor: "unset", color: "gray.500" }}
                  right={10}
                  onClick={() => {
                    setInputValue("");
                    submit();
                  }}
                />
              )}
            </InputRightElement>
          </InputGroup>
          <Select
            defaultValue="novas"
            w="200px"
            borderColor="secondary.normal"
            _hover={{ borderColor: "secondary.light", cursor: "pointer" }}
            _focus={{ boxShadow: "none" }}
            onChange={(e) => handleSort(e.target.value)}
          >
            <option value="antigas">Mais antigas</option>
            <option value="novas">Mais novas</option>
          </Select>
        </Flex>
      </Flex>
    </Container>
  );
};
