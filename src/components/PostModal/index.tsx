import {
  Button,
  Flex,
  Image,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  Text,
  VStack,
} from "@chakra-ui/react";
import { PostModalProps } from "../../services/interfaces";

export const PostModal = ({ post, close }: PostModalProps) => {
  return (
    <ModalContent padding={5}>
      <ModalCloseButton />
      <ModalBody>
        <Flex
          w="100%"
          h="max-content"
          justify="center"
          align="flex-start"
          boxSizing="border-box"
        >
          <Image
            src={post.imageUrl}
            alt={post.title}
            w={200}
            h={200}
            objectFit="cover"
            borderRadius={5}
          />
          <VStack alignItems="flex-start" spacing={5} marginLeft={5}>
            <VStack
              direction="column"
              textAlign="left"
              alignItems="flex-start"
              flex={1}
              spacing={2}
            >
              <Text fontWeight="bold" fontSize="1.3rem" lineHeight="1.5rem">
                {post.title}
              </Text>
              <Text fontSize="1rem" color="gray.500">
                {new Date(post.publishedAt).toLocaleDateString("pt-BR")}
              </Text>
              <Text color="gray.600" letterSpacing=".8px">
                {post.summary}
              </Text>
            </VStack>
            <Button onClick={() => close()}>Ir para o site</Button>
          </VStack>
        </Flex>
      </ModalBody>
    </ModalContent>
  );
};
