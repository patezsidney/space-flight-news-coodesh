import { Button, Container, Modal, ModalOverlay } from "@chakra-ui/react";
import { useState } from "react";
import { usePosts } from "../../providers/Posts";
import { Post, PostListProps } from "../../services/interfaces";
import { PostCard } from "../PostCard";
import { PostModal } from "../PostModal";

export const PostList = ({ handlePosts }: PostListProps) => {
  const { postList } = usePosts();
  const [postDetails, setPostDetails] = useState<Post>();
  const handleModal = () => {
    setPostDetails(undefined);
  };
  return (
    <Container maxW="1280px" textAlign="center" paddingBottom="100px">
      {postList.map((post, index) => (
        <PostCard
          key={post.id}
          post={post}
          indexOnList={index}
          showPostDetails={setPostDetails}
        />
      ))}
      <Button onClick={handlePosts} margin={20} size="lg">
        Mais postagens
      </Button>
      <Modal
        isOpen={!!postDetails}
        onClose={handleModal}
        blockScrollOnMount={false}
        size="3xl"
      >
        <ModalOverlay />
        {postDetails && <PostModal post={postDetails} close={handleModal} />}
      </Modal>
    </Container>
  );
};
