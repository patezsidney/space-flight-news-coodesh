import {
  Container,
  Flex,
  Link,
  VStack,
  Text,
  Image,
  Button,
} from "@chakra-ui/react";
import { PostCardProps } from "../../services/interfaces";

export const PostCard = ({
  post,
  indexOnList,
  showPostDetails,
}: PostCardProps) => {
  return (
    <Container
      w="100%"
      maxW="700px"
      margin="20px auto"
      padding="10px"
      border="1px solid"
      borderColor="gray.100"
      borderRadius={5}
    >
      <Flex
        w="100%"
        h="max-content"
        justify="center"
        align="center"
        direction={indexOnList % 2 !== 0 ? "row-reverse" : "row"}
      >
        <Image
          src={post.imageUrl}
          alt={post.title}
          w={200}
          h={200}
          borderRadius={5}
          objectFit="cover"
          marginRight={indexOnList % 2 !== 0 ? "0" : "20px"}
          marginLeft={indexOnList % 2 !== 0 ? "20px" : "0"}
        />
        <VStack
          alignItems="flex-start"
          marginRight={indexOnList % 2 !== 0 ? "20px" : "0"}
          marginLeft={indexOnList % 2 !== 0 ? "0" : "20px"}
          spacing={5}
        >
          <VStack
            direction="column"
            textAlign="left"
            alignItems="flex-start"
            flex={1}
            spacing={2}
          >
            <Text fontWeight="bold" fontSize="1.3rem" lineHeight="1.5rem">
              {post.title}
            </Text>
            <Flex w="100%" justify="space-between">
              <Text fontSize="1rem" color="gray.500">
                {new Date(post.publishedAt).toLocaleDateString("pt-BR")}
              </Text>
              <Link
                fontSize=".8rem"
                bgColor="secondary.normal"
                color="white"
                fontWeight="bold"
                padding="2px 5px"
                borderRadius={5}
                _hover={{ bgColor: "secondary.light" }}
              >
                {post.newsSite}
              </Link>
            </Flex>
            <Text
              color="gray.600"
              letterSpacing=".8px"
              overflow="hidden"
              textOverflow="ellipsis"
              lineHeight="1rem"
              maxH="3rem"
            >
              {post.summary}
            </Text>
          </VStack>
          <Button onClick={() => showPostDetails(post)}>Ver Mais</Button>
        </VStack>
      </Flex>
    </Container>
  );
};
