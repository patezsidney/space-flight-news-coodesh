import { ProviderProps } from "../services/interfaces";
import { PostProvider } from "./Posts";

const Providers = ({ children }: ProviderProps) => {
  return <PostProvider>{children}</PostProvider>;
};

export default Providers;
