import { createContext, useContext, useEffect, useState } from "react";
import { api } from "../../services/api";
import {
  Post,
  PostContextData,
  ProviderProps,
} from "../../services/interfaces";

const PostsContext = createContext<PostContextData>({} as PostContextData);

export const PostProvider = ({ children }: ProviderProps) => {
  const [postList, setPostList] = useState<Post[]>([]);
  const [nextPage, setNextPage] = useState<number>(10);
  const [sortByDate, setSortByDate] = useState<boolean>(false);

  const getPosts = async (filter: string = "") => {
    let response = false;
    let filterToRequest = !!filter ? `&title_contains=${filter}` : "";
    let sort = !!sortByDate ? "&_sort=publishedAt" : "";
    await api
      .get(`/articles?_limit=10${filterToRequest}${sort}`)
      .then((res) => {
        setPostList(res.data);
        setNextPage(nextPage + 10);
        response = true;
      })
      .catch((err) => console.log(err));
    return response;
  };

  const getMorePosts = async (filter: string = "") => {
    let response = false;
    let filterToRequest = !!filter ? `&title_contains=${filter}` : "";
    let sort = !!sortByDate ? "&_sort=publishedAt" : "";
    await api
      .get(`/articles?_limit=10&_start=${nextPage}${filterToRequest}${sort}`)
      .then((res) => {
        setPostList([...postList, ...res.data]);
        setNextPage(nextPage + 10);
        response = true;
      })
      .catch((err) => console.log(err));
    return response;
  };

  useEffect(() => {
    getPosts();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [sortByDate]);

  return (
    <PostsContext.Provider
      value={{
        postList,
        getMorePosts,
        getPosts,
        setSortByDate,
      }}
    >
      {children}
    </PostsContext.Provider>
  );
};

export const usePosts = () => useContext(PostsContext);
