import { extendTheme } from "@chakra-ui/react";

export const theme = extendTheme({
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  colors: {
    primary: {
      light: "#5C58A1",
      normal: "#302e53",
      dark: "#373561",
    },
    secondary: {
      light: "#DE7716",
      normal: "#D07014",
      dark: "#914E0F",
    },
  },
  components: {
    Button: {
      variants: {
        solid: {
          border: "none",
          bgColor: "primary.normal",
          color: "white",
          ":hover": {
            bgColor: "primary.light",
          },
          ":focus": {
            boxShadow: "none",
          },
        },
      },
      defaultProps: {
        variant: "solid",
      },
    },
  },
  styles: {
    global: {
      "html, body": {
        color: "#1E2022",
        boxShadow: "none",
        fontFamily: "Roboto Condensed, sans-serif",
      },
    },
  },
});
