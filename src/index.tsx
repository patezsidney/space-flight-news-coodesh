import React from "react";
import ReactDOM from "react-dom";
import App from "./App";
import { ChakraProvider } from "@chakra-ui/react";
import Providers from "./providers";
import { BrowserRouter } from "react-router-dom";
import { theme } from "./styles/theme";

ReactDOM.render(
  <React.StrictMode>
    <Providers>
      <ChakraProvider resetCSS theme={theme}>
        <BrowserRouter>
          <App />
        </BrowserRouter>
      </ChakraProvider>
    </Providers>
  </React.StrictMode>,
  document.getElementById("root")
);
