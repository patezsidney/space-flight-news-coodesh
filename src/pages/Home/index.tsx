import { FormEvent, useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Header } from "../../components/Header";
import { PostList } from "../../components/PostList";
import { usePosts } from "../../providers/Posts";
import { UseParams } from "../../services/interfaces";

const Home = () => {
  const params = useParams<UseParams>();
  const [inputValue, setInputValue] = useState<string>();

  const { getMorePosts, getPosts, setSortByDate } = usePosts();

  const handleSearch = (e?: FormEvent<HTMLDivElement>) => {
    if (!!e) {
      e.preventDefault();
      getPosts(inputValue);
    } else {
      getPosts();
    }
  };

  const handlePosts = () => {
    getMorePosts(inputValue);
  };

  const handleSort = (sort: string) => {
    if (sort === "antigas") {
      setSortByDate(true);
    }
    if (sort === "novas") {
      setSortByDate(false);
    }
  };

  useEffect(() => {
    if (!!params.query) {
      setInputValue(params.query);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <div>
      <Header
        inputValue={inputValue}
        setInputValue={setInputValue}
        submit={handleSearch}
        handleSort={handleSort}
      />
      <PostList handlePosts={handlePosts} />
    </div>
  );
};

export default Home;
