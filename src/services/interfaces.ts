import { FormEvent, ReactNode } from "react";

export interface UseParams {
  query?: string;
}

export interface LaunchesAndEvents {
  id: string;
  provider: string;
}

export interface Post {
  id: number;
  featured: boolean;
  title: string;
  url: string;
  imageUrl: string;
  newsSite: string;
  summary: string;
  publishedAt: string;
  launches: LaunchesAndEvents[];
  events: LaunchesAndEvents[];
}

export interface PostCardProps {
  showPostDetails: (post: Post) => void;
  indexOnList: number;
  post: Post;
}

export interface PostModalProps {
  close: () => void;
  post: {
    id: number;
    featured: boolean;
    title: string;
    url: string;
    imageUrl: string;
    newsSite: string;
    summary: string;
    publishedAt: string;
    launches: LaunchesAndEvents[];
    events: LaunchesAndEvents[];
  };
}

export interface PostListProps {
  handlePosts: () => void;
}

export interface HeaderProps {
  inputValue: string | undefined;
  setInputValue: (value: string) => void;
  submit: (e?: FormEvent<HTMLDivElement>) => void;
  handleSort: (sort: string) => void;
}

export interface PostContextData {
  postList: Post[];
  getMorePosts: (filter?: string) => Promise<boolean>;
  getPosts: (filter?: string) => Promise<boolean>;
  setSortByDate: (sort: boolean) => void;
}

export interface ProviderProps {
  children: ReactNode;
}
