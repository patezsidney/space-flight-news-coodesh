# Front-end Challenge 🏅 2021 - Space Flight News

> This is a challenge by [Coodesh](https://coodesh.com/)

Um projeto desenvolvido para consumir uma api e exibir uma lista de posts.

## Tecnologias utilizadas

- Framework
  - React.js
- Linguagens
  - Typescript (Javascript)
- Bibliotecas
  - Chakra-UI
  - React-Context
  - Axios
  - React-router-dom

## Instruções para utilização

1. Faça um clone do projeto em sua maquina
2. Instale as dependências (Utilizando `yarn` ou `mpm install`)
3. Inicie o servidor local do projeto (utilizando `yarn start` ou `npm start`)

Pronto, a aplicação ja deve estar rodando em localhost:3000.

## Funcionalidades

A aplicação lista os posts com 10 posts por página. para carregar outra página clique em mais posts no final da página.
Ao clicar em Ver Mais, será aberto um modal onde será possivem ver o post completo com o botão de redirecionamento para o site de origem (indisponível no momento).

### Filtros

è possível filtrar os resultados por palavras chave utilizando o campo de pesquisa no header ou por meio de url, por exemplo: ao acessar `localhost:3000/nasa` será filtrado com base na palavra chave 'nasa' passada na url.

Também é possivel alterar a ordem dos posts que por padrão são filtrados do mais novo para o mais antigo selecionando a opção desejada no campo do header.
